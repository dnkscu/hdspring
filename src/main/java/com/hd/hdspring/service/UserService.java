package com.hd.hdspring.service;
import com.hd.hdspring.dao.UserDAO;

import com.hd.hdspring.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserDAO userDAO;


    public boolean isExist(int username) {
        User user = getByEmployeeid(username);
        return null != user;
    }

    public User getByEmployeeid(int employeeid) {
        return userDAO.findByEmployeeid(employeeid);
    }


    public List<User> getByName(String name) {
        return userDAO.findByNameLike('%' + name + '%');
    }

    public User get(int username, String password) {
        return userDAO.getByEmployeeidAndPassword(username, password);
    }

    public void add(String name, String official, int deptid, double salary, String password, String phone) {
        User u = new User();
        u.setName(name);
        u.setOfficial(official);
        u.setDepartmentid(deptid);
        u.setSalary(salary);
        u.setSignin(0);
        u.setPassword(password);
        u.setPhonenumber(phone);
        userDAO.save(u);
    }

    public void addOrUpdate(User user){
        userDAO.save(user);
    }

    public  void  convertOfficial(int employeeid){
        User user = userDAO.findByEmployeeid(employeeid);
        user.setOfficial("正式");
        userDAO.save(user);
    }

    public  void  convertSalary(int employeeid,double salary){
        User user = userDAO.findByEmployeeid(employeeid);
        user.setSalary(user.getSalary()+salary);
        userDAO.save(user);
    }

    public  void  convertSignin(int employeeid){
        User user = userDAO.findByEmployeeid(employeeid);
        user.setSignin(user.getSignin()+1);
        user.setSalary(user.getSalary()+50);
        userDAO.save(user);
    }

    public  void  convertPassword(int employeeid,String password){
        User user = userDAO.findByEmployeeid(employeeid);
        user.setPassword(password);
        userDAO.save(user);
    }
    public  void  deleteByEmployeeid(int employeeid){
        userDAO.deleteByEmployeeid(employeeid);
    }

    public  List<User> list(){
        Sort sort = new Sort(Sort.Direction.ASC, "employeeid");
        return  userDAO.findAll(sort);
    }


    public List<User> listName(){
        return userDAO.findByNameLike("name");
    }

    public List<User> listOfficial(){
        return userDAO.findByOfficial("实习");
    }
}
