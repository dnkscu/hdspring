package com.hd.hdspring.service;
import com.hd.hdspring.dao.AssignDAO;
import com.hd.hdspring.dao.WorkDAO;
import com.hd.hdspring.pojo.Assign;
import com.hd.hdspring.pojo.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkService {
    @Autowired WorkDAO workDAO;
    @Autowired AssignDAO assignDAO;



    public boolean isExist(int workid) {
        Work work = getByWorkid(workid);
        return null != work;
    }
    public Work getByWorkid(int workid) {
        return workDAO.findByWorkid(workid);
    }
    public List<Work> getByWork(String starttime) {
        return workDAO.findByStarttimeLike('%' + starttime + '%');
    }

    public void deleteByWorkid(int workid){
        workDAO.deleteByworkid(workid);
    }

    public void insert(int workid,String name,String starttime,String describetion,String schedule) {
        Work work = workDAO.findByWorkid(workid);
        work.setName(name);
        work.setStarttime(starttime);
        work.setDescribetion(describetion);
        work.setSchedule(schedule);
        workDAO.save(work);
    }
    public int addwork(String name,String starttime,String describetion,String schedule){
        Work work =new Work();
        work.setName(name);
        work.setStarttime(starttime);
        work.setDescribetion(describetion);
        work.setSchedule(schedule);
        workDAO.save(work);
        return work.getWorkid();
    }
    public  void  convertSchedule(int workid,String schedule){
        Work work = workDAO.findByWorkid(workid);
        work.setSchedule(schedule);
        workDAO.save(work);
    }
    public List<Work> list(){
        Sort sort = new Sort(Sort.Direction.ASC, "workid");
        return  workDAO.findAll(sort);
    }

}
