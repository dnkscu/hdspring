package com.hd.hdspring.service;

import com.hd.hdspring.dao.DepartmentDAO;
import com.hd.hdspring.pojo.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {
    @Autowired
    DepartmentDAO departmentDAO;

    public List<Department> list(){
        Sort sort = new Sort(Sort.Direction.ASC, "departmentid");
        return departmentDAO.findAll(sort);
    }

    public boolean isExist(int id) {
        Department department = getByDepartmentid(id);
        return null != department;
    }

    public Department getByName(String name) {
        return departmentDAO.findByName(name);
    }
    public Department getByDepartmentid(int id) {
        return departmentDAO.findByDepartmentid(id);
    }


    public void add(String name) {
        Department department = new Department();
        department.setName(name);
        departmentDAO.save(department);
    }

    public void update(int id, String name){
        Department department = getByDepartmentid(id);
        department.setName(name);
        departmentDAO.save(department);
    }
    public void delete(int id){
        departmentDAO.deleteByDepartmentid(id);
    }
}
