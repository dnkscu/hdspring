package com.hd.hdspring.service;

import com.hd.hdspring.dao.ReceiverecordDAO;
import com.hd.hdspring.pojo.Receiverecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ReceiverecordService {
    @Autowired
    ReceiverecordDAO receiverecordDAO;

    public Receiverecord getByRecord(int id){
        return receiverecordDAO.findByRecord(id);
    }

    public void add(int deptid,int thingid,int number) {
    Receiverecord record = new Receiverecord();
    record.setDepartmentid(deptid);
    record.setThingid(thingid);
    record.setNumber(number);
    record.setBack(0);
    Date date = new Date();
    record.setTime(date);
    receiverecordDAO.save(record);
    }
 public List<Receiverecord> mydata(int id){
        return receiverecordDAO.findByDepartmentidAndBack(id,0);
 }
    public void back(int recordid) {
        Receiverecord record = getByRecord(recordid);
        record.setBack(1);
    }
    public boolean isExist(int id) {
        Receiverecord thing = getByRecord(id);
        return null != thing;
    }
}
