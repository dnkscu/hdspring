package com.hd.hdspring.service;
import com.hd.hdspring.dao.ThingDAO;
import com.hd.hdspring.pojo.Thing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ThingService {
    @Autowired
    ThingDAO thingDAO;

    public boolean isExist(String name) {
        Thing thing = getByName(name);
        return null != thing;
    }
    public boolean isExist(int id) {
        Thing thing = getByThingid(id);
        return null != thing;
    }

    public Thing getByName(String name) {
        return thingDAO.findByName(name);
    }
    public Thing getByThingid(int id){ return thingDAO.findByThingid(id);}

    public void add(int thingid,String name,int number) {
        Thing thing = new Thing();
        thing.setThingid(thingid);
        thing.setName(name);
        thing.setNumber(number);
        thingDAO.save(thing);
    }
    public List<Thing> listByNameLike(String name){
        return thingDAO.findAllByNameLike('%'+name+'%');
    }
  public void insert(String name,int number){
        Thing thing =new Thing();
        thing.setName(name);
        thing.setNumber(number);
        thingDAO.save(thing);
  }
    public void delete(int id){
        thingDAO.deleteById(id);
    }
    public List<Thing>  allData(){
        return thingDAO.findAll();
    }

}
