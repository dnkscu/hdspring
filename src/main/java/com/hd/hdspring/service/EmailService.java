package com.hd.hdspring.service;

import com.hd.hdspring.dao.EmailDAO;
import com.hd.hdspring.pojo.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailService {
    @Autowired
    EmailDAO emailDAO;

    public void insert(String content,int wid,int rid){
        Email email = new Email();
        email.setContent(content);
        email.setWritterid(wid);
        email.setReceiverid(rid);
        email.setIsdraft(1);
        email.setIsdelete(0);
        emailDAO.save(email);
    }

    public void update(Email email){
        Email email1 = getByEmailid(email.getEmailid());
        email1.setContent(email.getContent());
        emailDAO.save(email1);
    }
    public boolean isExist(int id){
        Email email = getByEmailid(id);
        return null != email;


    }
    public Email getByEmailid(int id){ return emailDAO.findByEmailid(id);}
    public void send(Email email1){
        Email email = emailDAO.findByEmailid(email1.getEmailid());
        email.setIsdraft(0);
        emailDAO.save(email);
    }
    public void delete(int emailid){
        Email email = emailDAO.findByEmailid(emailid);
        email.setIsdelete(1);
        emailDAO.save(email);
    }


    public List<Email> show(int id,int isdraft,int isdelete){
      return  emailDAO.findAllByWritteridAndIsdraftAndIsdelete(id,isdraft,isdelete);
    }


    public List<Email> show1(int id,int isdraft,int isdelete){
        return  emailDAO.findAllByReceiveridAndIsdraftAndIsdelete(id,isdraft,isdelete);
    }

}
