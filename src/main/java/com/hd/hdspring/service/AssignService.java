package com.hd.hdspring.service;
import com.hd.hdspring.pojo.Receiverecord;
import com.hd.hdspring.pojo.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.hd.hdspring.dao.AssignDAO;
import com.hd.hdspring.dao.WorkDAO;
import com.hd.hdspring.pojo.Assign;
import java.util.List;
@Service
public class AssignService {
    @Autowired
    AssignDAO assignDAO;
    @Autowired
    WorkDAO workDAO;

    public boolean isExist(int employeeid, int workid){
        Assign assign = assignDAO.findByEmployeeidAndWorkid(employeeid, workid);
        return null!=assign;
    }

    public List<Assign> list(){
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        return assignDAO.findAll(sort);
    }

    public void addassign(int workid,int employeeid){
        Assign assign = new Assign();
        assign.setWorkid(workid);
        assign.setEmployeeid(employeeid);
        assignDAO.save(assign);
    }
    public Assign getByWorkid(int workid){
        return assignDAO.findByWorkid(workid);
    }
    public void deleteByWorkid(int workid){
        assignDAO.deleteByWorkid(workid);
    }

    public List<Assign> listByEmployeeid(int employeeid){
        return assignDAO.findAllByEmployeeid(employeeid);
    }
}
