package com.hd.hdspring.service;
import com.hd.hdspring.dao.ContactRecordDAO;
import com.hd.hdspring.pojo.ContactRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ContactRecordService {
    @Autowired
    ContactRecordDAO contactRecordDAO;
    public boolean isExist(int username) {
        ContactRecord contactRecord = getByContactRecordid(username);
        return null != contactRecord;
    }

    public ContactRecord getByContactRecordid(int contactrecordid) {
        return contactRecordDAO.findByContactrecordid(contactrecordid);
    }

    public void add(int employeeid,int clientid, String time) {
        ContactRecord contactRecord = new ContactRecord();
        contactRecord.setEmployeeid(employeeid);
        contactRecord.setClientid(clientid);
        contactRecord.setTime(time);
        contactRecordDAO.save(contactRecord);
    }

    public void update(ContactRecord contactRecord) {
        contactRecordDAO.save(contactRecord);
    }

    public  void  deleteByContactRecordid(int contactrecordid){
        contactRecordDAO.deleteByContactrecordid(contactrecordid);
    }

    public  List<ContactRecord> list(){
        Sort sort = new Sort(Sort.Direction.ASC, "clientid");
        return  contactRecordDAO.findAll(sort);
    }

}


