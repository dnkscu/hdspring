package com.hd.hdspring.service;
import com.hd.hdspring.dao.ClientDAO;
import com.hd.hdspring.pojo.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {
    @Autowired
    ClientDAO clientDAO;

    public boolean isExist(int username) {
        Client client = getByClientid(username);
        return null != client;
    }

    public Client getByClientid(int clientid) {
        return clientDAO.findByClientid(clientid);
    }

    public void add(String name, String phone) {
        Client client = new Client();
        client.setName(name);
        client.setPhonenumber(phone);
        clientDAO.save(client);
    }

    public void update(Client client) {
        clientDAO.save(client);
    }

    public  void  deleteByClientid(int clientid){
        clientDAO.deleteByClientid(clientid);
    }

    public  List<Client> list(){
        Sort sort = new Sort(Sort.Direction.ASC, "clientid");
        return  clientDAO.findAll(sort);
    }

    public List<Client> listName(){
        return clientDAO.findByNameLike("name");
    }

    public List<Client> getByName(String name) {
        return clientDAO.findByNameLike('%' + name + '%');
    }


}
