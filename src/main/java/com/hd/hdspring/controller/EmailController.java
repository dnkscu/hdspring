package com.hd.hdspring.controller;

import com.hd.hdspring.pojo.Email;
import com.hd.hdspring.pojo.User;
import com.hd.hdspring.result.Result;
import com.hd.hdspring.result.ResultFactory;
import com.hd.hdspring.service.DepartmentService;
import com.hd.hdspring.service.EmailService;
import com.hd.hdspring.service.ReceiverecordService;
import com.hd.hdspring.service.ThingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
@Controller
public class EmailController {
    @Autowired
    EmailService emailService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    ReceiverecordService receiverecordService;
    @Autowired
    ThingService thingService;


    @PostMapping(value = "/api/writeemail")//输入一个email类型的数据，需要双方id和content属性有值就行，然后存入草稿箱
    @ResponseBody
    public Result writeemail(@RequestBody Email email) {

        emailService.insert(email.getContent(),email.getWritterid(),email.getReceiverid());
        return ResultFactory.buildSuccessResult("已经存入草稿箱");
    }

    @PostMapping(value = "/api/updateemail")//修改草稿箱的内容
    @ResponseBody
    public Result updateemail(@RequestBody Email email) {
        if(emailService.isExist(email.getEmailid())){
            emailService.update(email);
            return ResultFactory.buildSuccessResult("已经修改");
        }
        return ResultFactory.buildSuccessResult("修改错误");

    }

    @PostMapping(value = "/api/sendemail")//需要输入草稿箱里面的email数据，发送该email
    @ResponseBody
    public Result sendemail(@RequestBody Email email) {
        if(emailService.isExist(email.getEmailid())){
            emailService.send(email);
            return ResultFactory.buildSuccessResult("已经发送");
        }
       return  ResultFactory.buildFailResult("发送失败");
    }


    @PostMapping(value = "/api/showdraftemail")//需要输入当前User，只需要id有值，返回当前用户写的草稿内容
    @ResponseBody
    public Result showdraftemail(@RequestBody User user) {
        return ResultFactory.buildSuccessResult(emailService.show(user.getEmployeeid(),1,0));
    }

    @PostMapping(value = "/api/showsendemail")////需要输入当前User，只需要id有值，返回当前用户发送的内容
    @ResponseBody
    public Result showsendemail(@RequestBody User user) {
        return ResultFactory.buildSuccessResult(emailService.show(user.getEmployeeid(),0,0));
    }

    @PostMapping(value = "/api/showreceiveemail")//需要输入当前User，只需要id有值，返回当前用户收到的内容
    @ResponseBody
    public Result showreceiveemail(@RequestBody User user) {
        return ResultFactory.buildSuccessResult(emailService.show1(user.getEmployeeid(),0,0));
    }

    @PostMapping(value = "/api/deleteemail")//需要输入一个当前存在的email类型，删除该邮件
    @ResponseBody
    public Result deleteemail(@RequestBody Email email) {
        emailService.delete(email.getEmailid());
        return ResultFactory.buildSuccessResult("已经删除");
    }


    @PostMapping(value = "/api/showdeleteemail")//需要输入当前User，只需要id有值，返回当前用户删除的内容
    @ResponseBody
    public Result showdeleteemail(@RequestBody User user) {
        return ResultFactory.buildSuccessResult(emailService.show1(user.getEmployeeid(),0,1));
    }










}
