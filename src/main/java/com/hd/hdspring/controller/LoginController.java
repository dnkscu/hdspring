package com.hd.hdspring.controller;

import com.hd.hdspring.pojo.User;
import com.hd.hdspring.result.Result;
import com.hd.hdspring.result.ResultFactory;
import com.hd.hdspring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpSession;


@Controller
public class LoginController {

    @Autowired
    UserService userService;



    @PostMapping(value = "/api/login")
    @ResponseBody
    public Result login(@RequestBody User requestUser, HttpSession session) {

        User user = userService.get(requestUser.getEmployeeid(),requestUser.getPassword());
        if (null != user) {
            session.setAttribute("user", user);
            //System.out.println(session.getId());
            return ResultFactory.buildSuccessResult("登录成功");
        } else {
            return ResultFactory.buildFailResult("登录失败");
        }
    }

    @CrossOrigin
    @GetMapping(value = "/api/myself")
    @ResponseBody
    public Result Myself(HttpSession session){
        //System.out.println(session.getId());
        //HttpSession session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
        User user =(User)session.getAttribute("user");
        User u = userService.getByEmployeeid(user.getEmployeeid());
        return ResultFactory.buildSuccessResult(u);
    }

}

