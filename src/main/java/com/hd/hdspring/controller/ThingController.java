package com.hd.hdspring.controller;

import com.hd.hdspring.pojo.Department;
import com.hd.hdspring.pojo.Receiverecord;
import com.hd.hdspring.pojo.Thing;
import com.hd.hdspring.pojo.User;
import com.hd.hdspring.result.Result;
import com.hd.hdspring.result.ResultFactory;
import com.hd.hdspring.service.DepartmentService;
import com.hd.hdspring.service.ReceiverecordService;
import com.hd.hdspring.service.ThingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ThingController {
    @Autowired
    DepartmentService departmentService;
    @Autowired
    ReceiverecordService receiverecordService;
    @Autowired
    ThingService thingService;

    @CrossOrigin
    @PostMapping(value = "/api/buything")//部门购入物品
    @ResponseBody
    public Result buything(@RequestBody Thing thing) {
        Thing thing1 = new Thing();
        if (thingService.isExist(thing.getName())) {
            thing1 = thingService.getByName(thing.getName());
            thingService.add(thing1.getThingid(), thing1.getName(), thing1.getNumber() + thing.getNumber());
            return ResultFactory.buildSuccessResult("成功添加");
        } else{
            thingService.insert(thing.getName(), thing.getNumber());
            return ResultFactory.buildSuccessResult("已新增物品");

        }
    }

    @CrossOrigin
    @PostMapping(value = "/api/borrowthing")//部门借物品
    @ResponseBody
    public Result borrowthing(@RequestBody Receiverecord receiverecord) {
        if(!thingService.isExist(receiverecord.getThingid())){
            return ResultFactory.buildFailResult("没有此物品");
        }
        System.out.println(receiverecord.getDepartmentid());
       if(!departmentService.isExist(receiverecord.getDepartmentid())){
           return ResultFactory.buildFailResult("没有此部门");
       }
       Thing thing = thingService.getByThingid(receiverecord.getThingid());

        if (thingService.isExist(thing.getName())) {

            if(thing.getNumber()>=receiverecord.getNumber()) {
               thingService.add(thing.getThingid(), thing.getName(), thing.getNumber() - receiverecord.getNumber());
                receiverecordService.add(receiverecord.getDepartmentid(),thing.getThingid(),receiverecord.getNumber());
                return ResultFactory.buildSuccessResult("成功借出");
            }else{
                return ResultFactory.buildFailResult("库存不够");
            }
        } else{

            return ResultFactory.buildFailResult("没有此物品");

        }
    }



    @CrossOrigin
    @PostMapping(value = "api/deletething")//删除物品
    @ResponseBody
    public Result deletething(@RequestBody Thing thing) {
        if(thingService.isExist(thing.getThingid())){
            thingService.delete(thing.getThingid());
        }
    return ResultFactory.buildSuccessResult("已经删除");
    }


    @CrossOrigin
    @PostMapping(value = "/api/returnthing")//各部门根据借物品的record（int）还物品
    @ResponseBody
    public Result returnthing(@RequestBody Receiverecord receiverecord) {

        if(!receiverecordService.isExist(receiverecord.getRecord()) ){
            return ResultFactory.buildFailResult("不存在");
        }

        Receiverecord receiverecord1=receiverecordService.getByRecord(receiverecord.getRecord());
if(receiverecord1.getBack()==1){
    return ResultFactory.buildFailResult("你已经归还");
}else{

       receiverecordService.back(receiverecord.getRecord());
        Thing thing = thingService.getByThingid(receiverecord1.getThingid());
        thingService.add(receiverecord1.getThingid(),thing.getName(),thing.getNumber()+receiverecord1.getNumber());


    return ResultFactory.buildSuccessResult("成功归还");

    }
    }

    @CrossOrigin
    @GetMapping(value = "api/allfreething")//返回库存的办公物品
    @ResponseBody
    public Result allfreething() {
        return ResultFactory.buildSuccessResult(thingService.allData());
    }


    @GetMapping(value = "api/ourfreething")//返回本部门的办公物品
    @ResponseBody
    public Result ourfreething(HttpSession session) {
        User user = (User)session.getAttribute("user");
        return ResultFactory.buildSuccessResult(receiverecordService.mydata(user.getDepartmentid()));
    }

    @CrossOrigin
    @GetMapping(value = "api/searchthing")//根据名称查找
    @ResponseBody
    public Result searchthing(@RequestParam String name) {

        return ResultFactory.buildSuccessResult(thingService.listByNameLike(name));
    }



}
