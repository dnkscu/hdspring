package com.hd.hdspring.controller;

import com.hd.hdspring.pojo.ContactRecord;
import com.hd.hdspring.pojo.User;
import com.hd.hdspring.result.Result;
import com.hd.hdspring.result.ResultFactory;
import com.hd.hdspring.service.ContactRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
public class ContactRecordController {
    @Autowired
    ContactRecordService contactRecordService;

    @CrossOrigin
    @GetMapping(value = "/api/listcontact")
    @ResponseBody
    public  Result listcontact() {
        return  ResultFactory.buildSuccessResult(contactRecordService.list());
    }

    @CrossOrigin
    @PostMapping(value = "/api/addcontactrecord")
    @ResponseBody
    public  Result add(@RequestBody ContactRecord contactRecord, HttpSession session) {
        User user =(User)session.getAttribute("user");
        contactRecordService.add(user.getEmployeeid(),contactRecord.getClientid(),contactRecord.getTime());
        return  ResultFactory.buildSuccessResult("添加成功");
    }

    @CrossOrigin
    @PostMapping(value = "/api/deletecontactrecord")
    @ResponseBody
    @Transactional
    public  Result delete(@RequestBody ContactRecord contactRecord) {
        if (contactRecordService.isExist(contactRecord.getContactrecordid())){
            contactRecordService.deleteByContactRecordid(contactRecord.getContactrecordid());
            return  ResultFactory.buildSuccessResult("删除成功");
        }else {
            return ResultFactory.buildFailResult("无此记录");
        }
    }
}
