package com.hd.hdspring.controller;

import com.hd.hdspring.pojo.Department;
import com.hd.hdspring.result.Result;
import com.hd.hdspring.result.ResultFactory;
import com.hd.hdspring.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

@Controller
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    //显示所有部门
    @CrossOrigin
    @GetMapping(value = "/api/listdept")
    @ResponseBody
    public Result listdept(){
        return ResultFactory.buildSuccessResult(departmentService.list());
    }

    //添加一个部门
    @CrossOrigin
    @PostMapping(value = "/api/adddept")
    @ResponseBody
    @Transactional
    public Result adddept(@RequestBody Department department){
        if(departmentService.getByName(department.getName()) != null) {
            return ResultFactory.buildFailResult("该部门已经存在");
        }
        departmentService.add(department.getName());
        return ResultFactory.buildSuccessResult("已添加");
    }

    //删除一个部门
    @CrossOrigin
    @PostMapping(value = "/api/deletedept")
    @ResponseBody
    @Transactional
    public Result deletedept(@RequestBody Department department){
        departmentService.delete(department.getDepartmentid());
        return ResultFactory.buildSuccessResult("已删除");
    }

    //编辑部门名称
    @CrossOrigin
    @PostMapping(value = "/api/updatedept")
    @ResponseBody
    public Result updatedept(@RequestBody Department department){
        departmentService.update(department.getDepartmentid(), department.getName());
        return ResultFactory.buildSuccessResult("已修改");
    }
}
