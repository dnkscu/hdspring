package com.hd.hdspring.controller;

import com.hd.hdspring.pojo.Client;
import com.hd.hdspring.result.Result;
import com.hd.hdspring.result.ResultFactory;
import com.hd.hdspring.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
public class ClientController {
    @Autowired
    ClientService clientService;

    @CrossOrigin
    @GetMapping(value = "/api/listClient")
    @ResponseBody
    public Result list() {
        return  ResultFactory.buildSuccessResult(clientService.list());
    }

    @CrossOrigin
    @GetMapping(value = "/api/findClient")
    @ResponseBody
    public Result findByName(@RequestParam String name) {
        return ResultFactory.buildSuccessResult(clientService.getByName(name));
    }

    @CrossOrigin
    @PostMapping(value = "/api/add")
    @ResponseBody
    public  Result add(@RequestBody Client client) {
            clientService.add(client.getName(), client.getPhonenumber());
            return  ResultFactory.buildSuccessResult("添加成功");
    }

    @CrossOrigin
    @PostMapping(value = "/api/update")
    @ResponseBody
    public  Result update(@RequestBody Client client) {
        if (clientService.isExist(client.getClientid())){
            clientService.update(client);
            return  ResultFactory.buildSuccessResult("更新成功");
        }else {
            return ResultFactory.buildFailResult("无此客户");
        }
    }

    @CrossOrigin
    @PostMapping(value = "/api/deleteClient")
    @ResponseBody
    @Transactional
    public  Result delete(@RequestBody Client client) {
        if (clientService.isExist(client.getClientid())){
            clientService.deleteByClientid(client.getClientid());
            return  ResultFactory.buildSuccessResult("删除成功");
        }else {
            return ResultFactory.buildFailResult("无此客户");
        }
    }

}
