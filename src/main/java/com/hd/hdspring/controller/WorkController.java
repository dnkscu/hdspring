package com.hd.hdspring.controller;
import com.hd.hdspring.pojo.*;
import com.hd.hdspring.result.Result;
import com.hd.hdspring.result.ResultFactory;
import com.hd.hdspring.service.AssignService;
import com.hd.hdspring.service.WorkService;
import com.hd.hdspring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sun.security.provider.Sun;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Calendar;

@Controller
public class   WorkController {
    @Autowired
    WorkService workService;
    @Autowired
    UserService userService;
    @Autowired
    private AssignService assignService;

    @CrossOrigin
    @PostMapping(value = "/api/addwork")
    @ResponseBody
    public Result addWork(@RequestBody Work work,HttpSession session){
        User user = (User) session.getAttribute("user");
        int workid = workService.addwork(work.getName(), work.getStarttime(),work.getDescribetion(),work.getSchedule());
        assignService.addassign(workid,user.getEmployeeid());
        return ResultFactory.buildSuccessResult("已添加工作");


    }
    @CrossOrigin
    @PostMapping(value = "/api/insertwork")
    @ResponseBody
    public Result insertWork(@RequestBody Work work){
        if (workService.isExist(work.getWorkid())) {
            workService.insert(work.getWorkid(), work.getName(), work.getStarttime(),work.getDescribetion(),work.getSchedule());
            return ResultFactory.buildSuccessResult("已经修改该工作");
        } else{
            return ResultFactory.buildSuccessResult("没有该工作");
        }
    }
    @CrossOrigin
    @PostMapping(value = "/api/convertSchedule")
    @ResponseBody
    public  Result convertSchedule(@RequestBody Work work) {
        if (workService.isExist(work.getWorkid())){
            workService.convertSchedule(work.getWorkid(),work.getSchedule());
            return  ResultFactory.buildSuccessResult("更新进度成功");
        }else {
            return ResultFactory.buildFailResult("无此工作");
        }
    }
    @CrossOrigin
    @GetMapping(value = "/api/findwork")
    @ResponseBody
    public Result findByStarttime(@RequestParam String starttime) {
        return ResultFactory.buildSuccessResult(workService.getByWork(starttime));
    }

    @CrossOrigin
    @PostMapping(value = "/api/deletework")
    @ResponseBody
    @Transactional
    public  Result delete(@RequestBody Work work) {
        if (workService.isExist(work.getWorkid())){
            workService.deleteByWorkid(work.getWorkid());
            assignService.deleteByWorkid(work.getWorkid());
            return  ResultFactory.buildSuccessResult("已删除工作");
        }else {
            return ResultFactory.buildFailResult("无此工作");
        }
    }

    //我的工作
    @CrossOrigin
    @GetMapping(value = "api/mywork")
    @ResponseBody
    public Result mywork(HttpSession session){
        User user = (User) session.getAttribute("user");
        List<Assign> assigns = assignService.listByEmployeeid(user.getEmployeeid());
        if(assigns.isEmpty())
            return ResultFactory.buildFailResult("您没有工作安排");
        List<Integer> workids = assigns.stream().map(Assign::getWorkid).collect(Collectors.toList());
        List<Work> works = new ArrayList<>();
        for(int id : workids){
            works.add(workService.getByWorkid(id));
        }
        return ResultFactory.buildSuccessResult(works);
    }

    //我的工作根据星期分组
    @CrossOrigin
    @GetMapping(value = "api/worklist")
    @ResponseBody
    public Result worklist(HttpSession session){
        User user = (User) session.getAttribute("user");
        List<Assign> assigns = assignService.listByEmployeeid(user.getEmployeeid());
        if(assigns.isEmpty())
            return ResultFactory.buildFailResult("您没有工作安排");
        List<Integer> workids = assigns.stream().map(Assign::getWorkid).collect(Collectors.toList());
        List<Work> works = new ArrayList<>();
        for(int id : workids){
            works.add(workService.getByWorkid(id));
        }
        List<Work> Sunday = new ArrayList<>();
        List<Work> Monday = new ArrayList<>();
        List<Work> Tuesday = new ArrayList<>();
        List<Work> Wednesday = new ArrayList<>();
        List<Work> Thursday = new ArrayList<>();
        List<Work> Friday = new ArrayList<>();
        List<Work> Saturday = new ArrayList<>();
        for(Work work : works){
            String day = work.getStarttime();
            switch (day){
                case "星期一":
                    Monday.add(work);
                    break;
                case "星期二":
                    Tuesday.add(work);
                    break;
                case "星期三":
                    Wednesday.add(work);
                    break;
                case "星期四":
                    Thursday.add(work);
                    break;
                case "星期五":
                    Friday.add(work);
                    break;
                case "星期六":
                    Saturday.add(work);
                    break;
                case "星期日":
                    Sunday.add(work);
                    break;
            }
        }
        List<List> result = new ArrayList<>();
        result.add(Sunday);
        result.add(Monday);
        result.add(Tuesday);
        result.add(Wednesday);
        result.add(Thursday);
        result.add(Friday);
        result.add(Saturday);
        return  ResultFactory.buildSuccessResult(result);
    }

    //返回今天的工作
    @CrossOrigin
    @GetMapping(value = "api/todaywork")
    @ResponseBody
    public Result todaywork(HttpSession session) {
        User user = (User) session.getAttribute("user");
        List<Assign> assigns = assignService.listByEmployeeid(user.getEmployeeid());
        if (assigns.isEmpty())
            return ResultFactory.buildFailResult("您没有工作安排");
        List<Integer> workids = assigns.stream().map(Assign::getWorkid).collect(Collectors.toList());
        List<Work> works = new ArrayList<>();
        for (int id : workids) {

            works.add(workService.getByWorkid(id));
        }
        Calendar calendar = Calendar.getInstance();
        String []arr = {"星期日","星期一","星期二","星期三","星期四","星期五","星期六"};
        List<Work> today = new ArrayList<>();
        for(Work work : works){
            String day = work.getStarttime();
            if(day.equals(arr[calendar.get(Calendar.DAY_OF_WEEK)-1]))
                today.add(work);
        }
        return  ResultFactory.buildSuccessResult(today);
    }
}
