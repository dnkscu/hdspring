package com.hd.hdspring.controller;

import com.hd.hdspring.pojo.User;
import com.hd.hdspring.result.Result;
import com.hd.hdspring.result.ResultFactory;
import com.hd.hdspring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
public class UserControllor {
    @Autowired
    UserService userService;

    @CrossOrigin
    @GetMapping(value = "/api/List")
    @ResponseBody
    public Result list() {
        return  ResultFactory.buildSuccessResult(userService.list());
    }

    @CrossOrigin
    @GetMapping(value = "/api/find")
    @ResponseBody
    public Result findByName(@RequestParam String name) {
        return ResultFactory.buildSuccessResult(userService.getByName(name));
    }

    @CrossOrigin
    @GetMapping(value = "/api/findOfficial")
    @ResponseBody
    public Result findByOfficial() {
        return ResultFactory.buildSuccessResult(userService.listOfficial());
    }

    @CrossOrigin
    @PostMapping(value = "/api/User")
    @ResponseBody
    public  Result addOrUpdate(@RequestBody User user) {
        if (userService.isExist(user.getEmployeeid())){
            userService.addOrUpdate(user);
            return  ResultFactory.buildSuccessResult("更新成功");
        }else {
            userService.addOrUpdate(user);
            return  ResultFactory.buildSuccessResult("添加成功");
        }
    }

    @CrossOrigin
    @PostMapping(value = "/api/addEmployee")
    @ResponseBody
    public  Result addEmlpoyee(@RequestBody User user) {
        userService.add(user.getName(),user.getOfficial(), user.getDepartmentid(),user.getSalary(),user.getPassword(),user.getPhonenumber());
        return  ResultFactory.buildSuccessResult("添加成功");
    }

    @CrossOrigin
    @PostMapping(value = "/api/convertOfficial")
    @ResponseBody
    public  Result convertOfficial(@RequestBody User user) {
        if (userService.isExist(user.getEmployeeid())){
            userService.convertOfficial(user.getEmployeeid());
            return  ResultFactory.buildSuccessResult("转正成功");
        }else {
            return ResultFactory.buildFailResult("无此员工");
        }
    }

    @CrossOrigin
    @PostMapping(value = "/api/convertSalary")
    @ResponseBody
    public  Result convertSalary(@RequestBody User user) {
        if (userService.isExist(user.getEmployeeid())){
            userService.convertSalary(user.getEmployeeid(),user.getSalary());
            return  ResultFactory.buildSuccessResult("工资更新成功");
        }else {
            return ResultFactory.buildFailResult("无此员工");
        }
    }

    @CrossOrigin
    @PostMapping(value = "/api/convertSignin")
    @ResponseBody
    public  Result convertSignin(@RequestBody User user) {
        if (userService.isExist(user.getEmployeeid())){
            userService.convertSignin(user.getEmployeeid());
            return  ResultFactory.buildSuccessResult("签到成功");
        }else {
            return ResultFactory.buildFailResult("无此员工");
        }
    }

    @CrossOrigin
    @PostMapping(value = "/api/delete")
    @ResponseBody
    @Transactional
    public  Result delete(@RequestBody User user, HttpSession session) {
        User My = (User)session.getAttribute("user");
        if (user.getEmployeeid() == My.getEmployeeid())
            return ResultFactory.buildFailResult("不可删除自己");
        if (userService.isExist(user.getEmployeeid())){
            userService.deleteByEmployeeid(user.getEmployeeid());
            return  ResultFactory.buildSuccessResult("删除成功");
        }else {
            return ResultFactory.buildFailResult("无此员工");
        }
    }
    @CrossOrigin
    @PostMapping(value = "/api/convertPassword")
    @ResponseBody
    public  Result convertPassword(@RequestBody User user) {
        if (userService.isExist(user.getEmployeeid())){
            userService.convertPassword(user.getEmployeeid(),user.getPassword());
            return  ResultFactory.buildSuccessResult("密码修改成功");
        }else {
            return ResultFactory.buildFailResult("无此员工");
        }
    }
}
