package com.hd.hdspring.dao;
import com.hd.hdspring.pojo.ContactRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
public interface ContactRecordDAO extends JpaRepository<ContactRecord,Integer> {
    ContactRecord findByContactrecordid(int contactrecordid);
    void deleteByContactrecordid(int contactrecordid);
}