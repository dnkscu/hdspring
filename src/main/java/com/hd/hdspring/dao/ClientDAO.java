package com.hd.hdspring.dao;
import com.hd.hdspring.pojo.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
public interface ClientDAO extends JpaRepository<Client,Integer> {
    Client findByClientid(int clientid);
    void deleteByClientid(int clientid);
    List<Client> findByNameLike(String name);
}
