package com.hd.hdspring.dao;
import com.hd.hdspring.pojo.Thing;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
public interface ThingDAO extends JpaRepository<Thing,Integer>{
    Thing findByThingid(int thingid);
    Thing findByName(String name);

    List<Thing> findAllByNameLike(String name);

}
