package com.hd.hdspring.dao;
import com.hd.hdspring.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
public interface UserDAO extends JpaRepository<User,Integer> {
    User findByEmployeeid(int employee_id);
    User getByEmployeeidAndPassword(int employee_id,String password);
    void deleteByEmployeeid(int employeeid);
    List<User> findByNameLike(String name);
    List<User> findByOfficial(String official);
}
