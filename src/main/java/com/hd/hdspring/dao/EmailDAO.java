package com.hd.hdspring.dao;
import com.hd.hdspring.pojo.Email;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
public interface EmailDAO extends JpaRepository<Email,Integer> {
 Email findByEmailid(int id);

 List<Email> findAllByWritteridAndIsdraftAndIsdelete(int wid,int isdraft,int isdelete);
 List<Email> findAllByReceiveridAndIsdraftAndIsdelete(int rid,int isdraft,int isdelete);
}
