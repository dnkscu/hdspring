package com.hd.hdspring.dao;
import com.hd.hdspring.pojo.Department;
import org.springframework.data.jpa.repository.JpaRepository;
public interface DepartmentDAO extends JpaRepository<Department,Integer> {
    Department findByDepartmentid(int id);
    Department findByName(String name);
    void deleteByDepartmentid(int departmentid);
}
