package com.hd.hdspring.dao;

import com.hd.hdspring.pojo.Receiverecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReceiverecordDAO extends JpaRepository<Receiverecord,Integer> {
    Receiverecord findByDepartmentidAndThingid(int Deptid,int Thingid);
    Receiverecord findByRecord(int record);
    List<Receiverecord> findByDepartmentidAndBack(int id, int back);
}
