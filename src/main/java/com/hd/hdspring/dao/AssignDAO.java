package com.hd.hdspring.dao;
import com.hd.hdspring.pojo.Assign;
import com.hd.hdspring.pojo.Work;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
public interface AssignDAO extends JpaRepository<Assign, Integer>{
    Assign findByEmployeeidAndWorkid(int employeeid, int workid);
    List<Assign> findAllByEmployeeid(int employeeid);
    Assign findByWorkid(int workid);
    void deleteByWorkid(int workid);
}
