package com.hd.hdspring.dao;

import com.hd.hdspring.pojo.Work;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkDAO extends JpaRepository<Work,Integer> {
    Work findByWorkid(int workid);
    Work findByName(String name);
    List<Work> findByStarttimeLike(String starttime);
    void deleteByworkid(int workid);

}
