package com.hd.hdspring.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;


@Entity
@Table(name = "email")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Email {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emailid")
    int emailid;

    int writterid;

    int receiverid;

    String content;

    int isdraft;

    int isdelete;

    public int getEmailid() {
        return emailid;
    }

    public void setEmailid(int emailid) {
        this.emailid = emailid;
    }

    public int getWritterid() {
        return writterid;
    }

    public void setWritterid(int writterid) {
        this.writterid = writterid;
    }

    public int getReceiverid() {
        return receiverid;
    }

    public void setReceiverid(int receiverid) {
        this.receiverid = receiverid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getIsdelete() {
        return isdelete;
    }

    public int getIsdraft() {
        return isdraft;
    }

    public void setIsdelete(int isdelete) {
        this.isdelete = isdelete;
    }

    public void setIsdraft(int isdraft) {
        this.isdraft = isdraft;
    }
}
