package com.hd.hdspring.pojo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "contactrecord")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})

public class ContactRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contactid")
    int contactrecordid;

    public int getContactrecordid() {
        return contactrecordid;
    }

    public void setContactrecordid(int contactrecordid) {
        this.contactrecordid = contactrecordid;
    }


    int employeeid;

    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }

    int clientid;

    public int getClientid() {
        return clientid;
    }

    public void setClientid(int clientid) {
        this.clientid = clientid;
    }

    String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
