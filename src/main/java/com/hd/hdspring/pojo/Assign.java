package com.hd.hdspring.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "assign")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Assign {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "assignid")
    int assignid;
    int employeeid;
    int workid;
    public int getAssignid() {
        return assignid;
    }

    public void setAssignid(int assignid) {
        this.assignid = assignid;
    }
    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }


    public int getWorkid() {
        return workid;
    }
    public void setWokrid(int workid) {
        this.workid = workid;
    }
}
