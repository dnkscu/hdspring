package com.hd.hdspring.pojo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;

@Entity
@Table(name = "thing")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})

public class Thing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "thingid")
    int thingid;

    public int getThingid() {
        return thingid;
    }

    public void setThingid(int thingid) {
        this.thingid = thingid;
    }

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number=number;
    }
}
