package com.hd.hdspring.pojo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

        import javax.persistence.*;


@Entity
@Table(name = "employee")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employeeid")
    int employeeid;

    public int getEmployeeid() {
        return employeeid;
    }
    public void setEmployeeid(int data) {
        this.employeeid=data;
    }


    String password;

    public String getPassword() {
        return password;
    }
    public void setPassword(String data) {
        this.password=data;
    }

    String name;
    public String getName() {
        return name;
    }
    public void setName(String data) {
        this.name=data;
    }


    String official;

    public String getOfficial() {
        return official;
    }

    public void setOfficial(String official) {
        this.official = official;
    }

    int departmentid;

    public int getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(int departmentid) {
        this.departmentid = departmentid;
    }

    double salary;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    int signin;

    public int getSignin() {
        return signin;
    }

    public void setSignin(int signin) {
        this.signin = signin;
    }

    String phonenumber;

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    @Override
    public String toString() {
        return "User{" +
                "employeeid='" + employeeid + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", official='" + official + '\'' +
                ", departmentid='" + departmentid + '\'' +
                ", salary=" + salary +
                ", signin=" + signin +
                ", phonenumber='" + phonenumber + '\'' +
                '}';
    }
}
