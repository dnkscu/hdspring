package com.hd.hdspring.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "receiverecord")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Receiverecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "record")
    int record;

    public int getRecord() {
        return record;
    }

    public void setRecord(int record) {
        this.record = record;
    }

    int departmentid;
    int thingid;
    int number;
    Date time;
    int back;

    public int getBack() {
        return back;
    }

    public void setBack(int back) {
        this.back = back;
    }

    public void setDepartmentid(int departmentid) {
        this.departmentid = departmentid;
    }

    public int getDepartmentid() {
        return departmentid;
    }

    public int getThingid() {
        return thingid;
    }

    public void setThingid(int thingid) {
        this.thingid = thingid;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
