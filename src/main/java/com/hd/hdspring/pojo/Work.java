package com.hd.hdspring.pojo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hd.hdspring.service.WorkService;

import javax.persistence.*;

@Entity
@Table(name = "work")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Work {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "workid")
    int workid;

    public int getWorkid() {
        return workid;
    }

    public void setWorkid(int workid) {
        this.workid = workid;
    }

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String describetion;

    public String getDescribetion() {
        return describetion;
    }

    public void setDescribetion(String describetion) {
        this.describetion = describetion;
    }


    String schedule;

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }


    String starttime;

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

}
