package com.hd.hdspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HdspringApplication {



    public static void main(String[] args) {
        SpringApplication.run(HdspringApplication.class, args);
    }

}
